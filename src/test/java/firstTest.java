import org.testng.annotations.Test;
import Pages.baseTestClass;

import static com.codeborne.selenide.Selenide.open;
import static Pages.pageWithSelenide.*;
import static helpers.testData.*;

public class firstTest extends baseTestClass {

    @Test(description = "Задание 1", priority = 0)
    public void testFirstSelenide() {
        //Открываем страницу сайта
        openMainPage();

        //Вводим данные
        enterField(fieldName, rightLogin);

        enterField(fieldPassword, rightPassword);

        //Подтверждаем вход
        //clickButton(fieldPassword);
        pressEnterForElement(fieldPassword);

        //Наблюдаем корректный результат
        checkElementVisible(successLogin);

        //Тест завершен
        System.out.println("First task - done");
    }

    @Test(description = "Задание 2", priority = 1)
    public void testSecondSelenide() {
        //Открываем страницу сайта
        openMainPage();

        //Вводим данные
        enterField(fieldName, wrongLogin);

        enterField(fieldPassword, rightPassword);

        //Подтверждаем вход
        pressEnterForElement(fieldPassword);

        //Наблюдаем корректный результат
        checkElementVisible(wrongLoginName);

        //Тест завершен
        System.out.println("Second task - done");
    }

    @Test(description = "Задание 3", priority = 2)
    public void testThirdSelenide() {
        //Открываем страницу сайта
        openMainPage();

        //Вводим данные
        enterField(fieldName, rightLogin);

        enterField(fieldPassword, wrongPassword);

        //Подтверждаем вход
        pressEnterForElement(fieldPassword);

        //Наблюдаем корректный результат
        checkElementVisible(wrongLoginPassword);

        //Тест завершен
        System.out.println("Third task - done");
    }

}

package Pages;

import com.codeborne.selenide.Configuration;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import static com.codeborne.selenide.WebDriverRunner.closeWebDriver;

public class baseTestClass {
    @BeforeMethod
    public static void beforeMethod() {

        Configuration.browser = myChromeBrowserClass.class.getName();
        Configuration.startMaximized = true;
        System.out.println("LocalWebDriver");
        Configuration.reportsFolder = "target/reports";
        Configuration.screenshots = false;

    }

    @AfterMethod
    public void afterMethod() {

        closeWebDriver();
    }
}

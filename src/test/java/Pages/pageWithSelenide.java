package Pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import static com.codeborne.selenide.Condition.exist;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.WebDriverRunner.getWebDriver;
import static helpers.testData.*;

public class pageWithSelenide {
    public static SelenideElement fieldName = $(By.name("username"));
    public static SelenideElement fieldPassword = $(By.name("password"));
    public static SelenideElement enterLogin = $(By.id("//*[@id=\\\"login\\\"]/button/i"));
    public static SelenideElement successLogin = $(By.xpath("//*[contains(text(),'Welcome to the Secure Area. When you are done click logout below.')]"));
    public static SelenideElement wrongLoginName = $(By.xpath("//*[contains(text(),'Your username is invalid!')]"));
    public static SelenideElement wrongLoginPassword = $(By.xpath("//*[contains(text(),'Your password is invalid!')]"));

    //Открыть главную страницу
    public static void openMainPage() {
        open(url);
    }

    //Ожидание пока элемент появится
    public static void waitForElementToAppear(SelenideElement element) {
        element.waitUntil(Condition.appear, 30000);
    }

    //Заполнение поля
    public static void enterField(SelenideElement element, String value) {
        element.waitUntil(Condition.appear, 10000);
        element.setValue("");
        element.setValue(value);
    }

    //Клик на кнопку
    public static void clickButton(SelenideElement element) {
        JavascriptExecutor executor = (JavascriptExecutor) getWebDriver();
        executor.executeScript("arguments[0].click();", element);
    }

    public static void pressEnterForElement(SelenideElement element) {
        element.sendKeys(Keys.ENTER);
    }

    public static SelenideElement checkElementVisible(SelenideElement element) {
        return element.shouldBe(Condition.visible);
    }
}
